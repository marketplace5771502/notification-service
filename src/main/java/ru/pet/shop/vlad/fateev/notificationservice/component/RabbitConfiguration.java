package ru.pet.shop.vlad.fateev.notificationservice.component;

import org.springframework.amqp.core.AmqpAdmin;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.rabbit.connection.CachingConnectionFactory;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitAdmin;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RabbitConfiguration {
    @Bean
    public ConnectionFactory connectionFactory() {
        return new CachingConnectionFactory("localhost");
    }

    @Bean
    public ConnectionFactory cwsConnectionFactory() {
        CachingConnectionFactory cwsConnectionFactory = new CachingConnectionFactory();
        cwsConnectionFactory.setUsername("guest");
        cwsConnectionFactory.setPassword("guest");
        cwsConnectionFactory.setAddresses("localhost:5672");

        return cwsConnectionFactory;
    }

    @Bean
    public AmqpAdmin admin() {
        return new RabbitAdmin(cwsConnectionFactory());
    }

    @Bean
    public AmqpTemplate template() {
        return new RabbitTemplate(cwsConnectionFactory());
    }

    @Bean
    public Queue emailQueue() {
        return new Queue("email");
    }
}
