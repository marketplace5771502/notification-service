package ru.pet.shop.vlad.fateev.notificationservice.component;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.annotation.EnableRabbit;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;
import ru.pet.shop.vlad.fateev.notificationservice.serivce.EmailSenderService;

@Component
@EnableRabbit
@Slf4j
@AllArgsConstructor
public class ListenerRabbit {

    private final EmailSenderService emailSenderService;

    @RabbitListener(queues = "email")
    public void processListEmail(String message) {
        emailSenderService.sendEmailApi(message, "Test",
                "Вы зарегестрировались в моём Pet-Project");
        log.info(message);
    }
}
