package ru.pet.shop.vlad.fateev.notificationservice.serivce;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

@Service
@Slf4j
@AllArgsConstructor
public class EmailSenderService {

    private final JavaMailSender sender;

    public void sendEmailApi(String email,
                             String subject,
                             String body) {

        SimpleMailMessage message = new SimpleMailMessage();
        message.setFrom("vladislavvfateevv@gmail.com");
        message.setText(body);
        message.setSubject(subject);
        message.setTo(email);
        sender.send(message);
        log.info("Сообщение отправленно на почту!");

    }
}
